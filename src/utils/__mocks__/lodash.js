export function isEmpty(obj) {
	return obj.length === 0;
}