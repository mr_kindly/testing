import { getId } from './index';

describe('Utils', () => {
	describe('getId', () => {
		let list;
		beforeAll(() => {
			list = [{
				id: 1,
			}, {
				id: 4
			}, {
				id: 3
			}];
		})
		it('should generate new id more than all of previous', () => {
			const newId = getId(list);
			expect(newId).toBe(5);
		});

		it('should always generate new id', () => {
			const newId = getId(list);
			list.push({ id: newId });
			const newestId = getId(list);
			expect(newId).not.toBe(newestId);
			expect(newestId).toBeGreaterThan(newId);
			expect(newId).toBeLessThan(newestId);
		});
	});
});
