export default function(list) {
	let max = 0;
	list.forEach((element) => {
		if (element.id > max) {
			max = element.id;
		}
	});
	return max + 1;
};