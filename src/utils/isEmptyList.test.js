import { isEmptyList } from './index';

// jest.mock('lodash', () => ({
// 	isEmpty: (obj) => obj.length === 0
// }));

describe('Utils', () => {
	describe('isEmptyList', () => {
		it('should return false if list is not empty', () => {
			const list = [{
				id: 1,
			}, {
				id: 4
			}, {
				id: 3
			}];
			expect(isEmptyList(list)).toBeFalsy();
		});

		it('should return true if list is empty', () => {
			const list = [];
			expect(isEmptyList(list)).toBeTruthy();
		});
	});
});
