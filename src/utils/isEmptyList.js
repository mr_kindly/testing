import {isEmpty} from 'lodash';

export default function(list) {
	return isEmpty(list);
};