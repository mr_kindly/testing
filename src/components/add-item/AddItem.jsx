import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { getId } from './../../utils';

class AddItem extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			active: false,
		};
	}

	handleActionClick = () => {
		const {
			action, todoList, categoryList, onButtonClick,
		} = this.props;
		this.setState({ active: !(this.input.value && this.input.value.trim()) });
		if (action.idCategory) {
			const index = action.idCategory.location.pathname.lastIndexOf('categories/');
			if (index !== -1) {
				const id = getId(todoList);
				const idCat = parseInt(action.idCategory.location.pathname.substr(index + 11), 10);
				if (categoryList.find(item => item.id === idCat)) {
					onButtonClick({ id, title: this.input.value, idCategory: idCat });
				}
			}
		} else {
			const id = getId(categoryList);
			onButtonClick({ id, title: this.input.value });
		}
		this.input.value = '';
	};

	render() {
		const { action } = this.props;
		const { active } = this.state;
		return (

			<div className="input-group" id={action.id}>
				<input type="text" required="required" placeholder={action.title} ref={(input) => { this.input = input; }} className="form-control" />
				<span className="input-group-btn">
					<button onClick={this.handleActionClick} className="btn btn-primary" type="button" title="Click me to clear the input field">
						{action.action}
					</button>
				</span>
			</div>
		);
	}
}
AddItem.propTypes = {
	action: PropTypes.objectOf(PropTypes.object),
	todoList: PropTypes.arrayOf(PropTypes.array),
	categoryList: PropTypes.arrayOf(PropTypes.array),
	onButtonClick: PropTypes.objectOf(PropTypes.object),
};

AddItem.defaultProps = {
	action: {},
	todoList: [],
	categoryList: [],
	onButtonClick: {},
};

const mapStateToProps = store => ({
	categoryList: store.category.items,
	error: store.category.error,
	todoList: store.todo.items,
});

export default withRouter(connect(
	mapStateToProps,
)(AddItem));
