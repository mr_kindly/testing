import React from 'react';
import './pageNotFound.scss';

const PageNotFound = () => (
  <div className="page-not-found">
    <p>Sorry, Page Not Found</p>
  </div>
);

export default PageNotFound;
