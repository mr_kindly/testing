import * as actionTypes from './actiontypes';

const addTodoItem = todo => ({
  type: actionTypes.ADD_TODO_ITEM,
  payload: todo,
});
const editTodoItem = todo => ({
  type: actionTypes.EDIT_TODO_ITEM,
  payload: todo,
});
const deleteTodoItems = id => ({
  type: actionTypes.REMOVE_ITEMS_CATEGORY,
  payload: id,
});
const toggleTodoItem = id => ({
  type: actionTypes.TOGGLE_ITEMS,
  payload: id,
});
const searchTodoItem = str => ({
  type: actionTypes.SEARCH_ITEMS,
  payload: str,
});

export {
  addTodoItem, editTodoItem, deleteTodoItems, toggleTodoItem, searchTodoItem,
};
