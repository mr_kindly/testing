import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { withRouter } from 'react-router';
import { toggleTodoItem } from './todoActions';
import { changeDoneCategoryItem } from '../category/categoryActions';
import './toDo.scss';

class ToDo extends React.Component {
	handleChangeChecked = () => {
		const {
			todo, toggleTodo, todos, changeDoneCategory,
		} = this.props;
		toggleTodo(todo.id);
		const isDone = todos.some(item => (item.idCategory === todo.idCategory
			&& !item.checked));
		changeDoneCategory({
			id: todo.idCategory,
			isDone: isDone,
		});
	};

	render() {
		const { todo } = this.props;
		return (
			<div className="row todo">
				<div className="col-xs-1">
					<input className="todo-is-done" type="checkbox" name="checkedToDoElem" defaultChecked={todo.checked} onClick={this.handleChangeChecked} />
				</div>
				<div className="col-xs-10">
					<p className="do-elem-content">{todo.title}</p>
				</div>
				<div className="col-xs-1">
					<Link to={`/categories/${todo.idCategory}/todolist/edit/${todo.id}`} className="link todo-edit">
						<i className="glyphicon glyphicon-edit" />
					</Link>
				</div>
			</div>
		);
	}
}
ToDo.propTypes = {
	todo: PropTypes.objectOf(PropTypes.object),
	todos: PropTypes.arrayOf(PropTypes.array),
	toggleTodo: PropTypes.func,
	changeDoneCategory: PropTypes.func,
};

ToDo.defaultProps = {
	todos: [],
	toggleTodo: () => undefined,
	changeDoneCategory: () => undefined,
};

const mapStateToProps = store => ({
	todos: store.todo.items,
});

const mapDispatchToProps = dispatch => ({
	toggleTodo: item => dispatch(toggleTodoItem(item)),
	changeDoneCategory: item => dispatch(changeDoneCategoryItem(item)),
});

export default withRouter(connect(
	mapStateToProps,
	mapDispatchToProps,
)(ToDo));
