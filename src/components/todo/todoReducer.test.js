/* eslint-disable no-undef */
import reducer from './todoReducer';

describe('todos reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(
      {
        search: '',
        items: [
          {
            id: 1,
            idCategory: 1,
            title: 'Typing and checking value in search input',
            description: 'nothing',
            checked: false,
          },
          {
            id: 2,
            idCategory: 1,
            title: 'Apply search',
            description: 'Apply search and checking url',
            checked: false,
          },
          {
            id: 3,
            idCategory: 1,
            title: 'Apply search and show results',
            description: 'Apply search and checking results',
            checked: false,
          },
        ],
      },
    );
  });
});
