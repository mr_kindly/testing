/* eslint-disable no-param-reassign */
import * as actionTypes from './actiontypes';

const initialState = {
  search: '',
  items: [
    {
      id: 1,
      idCategory: 1,
      title: 'Typing and checking value in search input',
      description: 'nothing',
      checked: false,
    },
    {
      id: 2,
      idCategory: 1,
      title: 'Apply search',
      description: 'Apply search and checking url',
      checked: false,
    },
    {
      id: 3,
      idCategory: 1,
      title: 'Apply search and show results',
      description: 'Apply search and checking results',
      checked: false,
    },
  ],
};

export default function todoReducer(state = initialState, action) {
  switch (action.type) {
    case actionTypes.ADD_TODO_ITEM:
    {
      const newState = {
        items: [
          {
            id: action.payload.id,
            idCategory: action.payload.idCategory,
            title: action.payload.title,
            description: 'nothing',
            checked: false,
          },
          ...state.items],
      };
      return newState;
    }
    case actionTypes.EDIT_TODO_ITEM:
    {
      const updatedItems = state.items.map((item) => {
        if (item.id === action.payload.id) {
          item = action.payload;
        }
        return item;
      });
      return {
        items: [...updatedItems],
      };
    }
    case actionTypes.REMOVE_ITEMS_CATEGORY:
    {
      const filterArray = state.items.filter(category => category.idCategory !== action.payload);
      return {
        items: filterArray,
      };
    }
    case actionTypes.TOGGLE_ITEMS:
    {
      return {
        ...state,
        items: state.items.map(item => (item.id === action.payload
          ? { ...item, checked: !item.checked } : item)),
      };
    }
    case actionTypes.SEARCH_ITEMS:
    {
      return {
        ...state,
        search: action.payload,
      };
    }
    default:
      return state;
  }
}
