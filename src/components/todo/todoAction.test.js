/* eslint-disable no-undef */
import * as actions from './todoActions';
import * as types from './actiontypes';

describe('Actions', () => {
	describe('Todo', () => {
		it('search action should return payload with "search" substring', () => {
			const searchStr = 'Search query';
			expect(actions.searchTodoItem(searchStr).payload).toMatch(/search/ig);
		});

		it('should create an action to toggle a todo', () => {
			const id = 1;
			const expectedAction = {
				type: types.TOGGLE_ITEMS,
				payload: id,
			};
			expect(actions.toggleTodoItem(id)).toEqual(expectedAction);
		});

		it('should create an action to remove a todo', () => {
			const id = 1;
			const expectedAction = {
				type: types.REMOVE_ITEMS_CATEGORY,
				payload: id,
			};
			expect(actions.deleteTodoItems(id)).toEqual(expectedAction);
		});
	})

});
