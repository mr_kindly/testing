/* eslint-disable react/jsx-filename-extension */
/* eslint-disable no-undef */
import React from 'react';
import renderer from 'react-test-renderer';
import Title from './Title';

describe('Title', () => {
  it('render Title', () => {
    const tree = renderer
      .create(<Title />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
