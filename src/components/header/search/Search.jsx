/* eslint-disable react/button-has-type */
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { searchTodoItem } from '../../todo/todoActions';
import { toggleShowDoneItem } from '../../category/categoryActions';
import './search.scss';

class Search extends React.Component {
	handleActionShowDoneClick = () => {
		const { toggleShowDone, showDone } = this.props;
		toggleShowDone(showDone);
	}

	handleActionChange = () => {
		// eslint-disable-next-line react/prop-types
		const { history, searchTodo } = this.props;
		history.push(`/search/${this.inputSearch.value}`);
		searchTodo(this.inputSearch.value);
	}

	render() {
		const { showDone } = this.props;
		return (
			<div>
				<form className="navbar-form navbar-right search-form" onSubmit={this.handleActionChange}>
					<input className="form-control search-control" required="required" placeholder="Seach" ref={(input) => { this.inputSearch = input; }} />
				</form>
			</div>
		);
	}
}


Search.propTypes = {
	showDone: PropTypes.bool,
	searchTodo: PropTypes.func,
	toggleShowDone: PropTypes.func,
};
Search.defaultProps = {
	showDone: false,
	searchTodo: () => undefined,
	toggleShowDone: () => undefined,
};

const mapStateToProps = store => ({
	showDone: store.category.showDone,
});

const mapDispatchToProps = dispatch => ({
	searchTodo: item => dispatch(searchTodoItem(item)),
	toggleShowDone: item => dispatch(toggleShowDoneItem(item)),
});

export default withRouter(connect(
	mapStateToProps,
	mapDispatchToProps,
)(Search));
