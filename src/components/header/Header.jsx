import React from 'react';
import Title from './title/Title';
import Search from './search/Search';
import './header.scss';

function Header() {
	return (
		<nav className="navbar navbar-default">
			<div className="container-fluid">
				<div className="navbar-header">
					<Title />
				</div>
				<Search />
			</div>
		</nav>
	);
}
export default Header;
