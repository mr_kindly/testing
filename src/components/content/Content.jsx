import React from 'react';
import { Route, Switch } from 'react-router-dom';
import CategoryList from '../categoty-list/CategoryList';
import ToDoList from '../todo-list/ToDoList';
import EditTodo from '../edit-todo/EditToDo';
import EmptyPage from '../empty-page/EmptyPage';
import './content.scss';


const Content = () => (
  <div className="content">
    <section className="categories-aside">
      <Switch>
        <Route exact path="/" component={CategoryList} />
        <Route exact path="/categories/showDone=:bool" component={CategoryList} />
        <Route component={CategoryList} />
      </Switch>
    </section>
    <section className="todo-wrapper">
      <Route exact path="/" component={EmptyPage} />
      <Route exact path="/categories/:id" component={ToDoList} />
      <Route exact path="/search/:subString" component={ToDoList} />
      <Route path="/categories/:idCategories/todolist/edit/:id" component={EditTodo} />
    </section>
  </div>
);

export default Content;
