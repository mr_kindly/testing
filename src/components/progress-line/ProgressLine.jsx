/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';

class ProgressLine extends React.Component {
	render() {
		const { currentCategory, todos } = this.props;
		let count = 0;
		let isDone = 0;
		if (todos) {
			todos.forEach((element) => {
				if (currentCategory === 0 || element.idCategory === currentCategory) {
					count += 1;
					if (element.checked) {
						isDone += 1;
					}
				}
			});
		}
		const style = {
			width: `${(isDone / count) * 100}%`
		}
		return (
			<div className="progress">
				<div className="progress-bar progress-bar-success progress-bar-striped" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style={style}>
					<span className="sr-only">40% Complete (success)</span>
				</div>
			</div>
		);
	}
}

ProgressLine.propTypes = {
	currentCategory: PropTypes.objectOf(PropTypes.object),
	todos: PropTypes.arrayOf(PropTypes.array),
};

ProgressLine.defaultProps = {
	currentCategory: {},
	todos: [],
};


const mapStateToProps = store => ({
	currentCategory: store.category.currentCategory,
	todos: store.todo.items,
});

export default withRouter(connect(
	mapStateToProps,
)(ProgressLine));
