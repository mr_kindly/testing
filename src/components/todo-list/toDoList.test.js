/* eslint-disable react/jsx-filename-extension */
/* eslint-disable no-undef */
import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, configure } from 'enzyme';

import ToDoList from './ToDoList';

configure({ adapter: new Adapter() });

describe('Category List', () => {
  it('should be render text Todos is empty', () => {
    const todos = [];
    const component = shallow(<ToDoList todoList={todos} />);
    expect(component).toMatchSnapshot();
  });
  it('should be render two elements', () => {
    const todos = [{ id: 1 }, { id: 2 }];
    const component = shallow(<ToDoList todoList={todos} />);
    expect(component).toMatchSnapshot();
  });
});
