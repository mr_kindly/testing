/* eslint-disable react/prefer-stateless-function */
import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import ToDo from '../todo/ToDo';
import { editTodoItem } from '../todo/todoActions';
import { isEmptyList } from './../../utils';

class ToDoList extends React.Component {
	render() {
		const { todo, editTodo, match } = this.props;
		let todoElements;
		const subSting = match ? match.params.subString : todo.search;
		let filterArray = [];
		if (subSting) {
			filterArray = todo.items.filter(obj => obj.title.indexOf(subSting) !== -1)
				.map(obj => obj);
			todoElements = filterArray.map(t => (
				<li className="list-group-item" key={t.id}>
					<ToDo todo={t} onButtonClick={editTodo} />
				</li>
			));
		} else {
			const idCategory = parseInt(match.params.id, 10);
			filterArray = todo.items.filter(obj => obj.idCategory === idCategory)
				.map(obj => obj);
			todoElements = filterArray.map(doelem => (
				<li className="list-group-item" key={doelem.id}>
					<ToDo todo={doelem} onButtonClick={editTodo} />
				</li>
			));
		}
		return (
			<div>
				{
					!isEmptyList(filterArray) && <ul className="list-group">
						{todoElements}
					</ul>
				}
				{
					isEmptyList(filterArray) && <h3>Search result is empty</h3>
				}

			</div>
		);
	}
}
ToDoList.propTypes = {
	todo: PropTypes.objectOf(PropTypes.object),
	match: PropTypes.objectOf(PropTypes.object),
	editTodo: PropTypes.func,
};

ToDoList.defaultProps = {
	todo: {},
	match: {},
	editTodo: () => undefined,
};

export const mapStateToProps = store => ({
	todo: store.todo,
});

export const mapDispatchToProps = dispatch => ({
	editTodo: item => dispatch(editTodoItem(item)),
});

export default withRouter(connect(
	mapStateToProps,
	mapDispatchToProps,
)(ToDoList));
