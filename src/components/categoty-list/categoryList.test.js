/* eslint-disable react/jsx-filename-extension */
import React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { shallow, configure } from 'enzyme';

import CategoryList from './CategoryList';

configure({ adapter: new Adapter() });

describe('Category List', () => {
  it('should be render text Todos is empty', () => {
    const categories = [];
    const component = shallow(<CategoryList categories={categories} />);
    expect(component).toMatchSnapshot();
  });
  it('should be render two elements', () => {
    const categories = [{ id: 1 }, { id: 2 }];
    const component = shallow(<CategoryList categories={categories} />);
    expect(component).toMatchSnapshot();
  });
});
