import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Link, withRouter } from 'react-router-dom';
import Category from '../category/Category';
import { deleteTodoItems } from '../todo/todoActions';
import {
  deleteCategoryItem, editCategoryItem, plusCategoryItem, changeCurrentCategoryItem,
} from '../category/categoryActions';
import './categotyList.scss';


class CategoryList extends React.Component {
  isMatchParamTrue = param => param === 'true';

  handleActionChangeCurrent = (e) => {
    const { changeCurrentCategory } = this.props;
    changeCurrentCategory(parseInt(e.target.id, 10));
  };

  render() {
    const {
      category, deleteCategory, deleteTodos, editCategory, plusCategory, match,
    } = this.props;
    const filter = this.isMatchParamTrue(match.params.bool) || category.showDone;
    const categoryElements = category.items.filter(item => item.idParent === 0
      && item.isDone === filter).map(item => (
        <li key={item.id}>
          <Link to={`/categories/${item.id}`} className="link" onClick={this.handleActionChangeCurrent}>
            <Category
              category={item}
              onButtonClick={{
                deleteCategory, deleteTodos, editCategory, plusCategory,
              }}
            />
          </Link>
        </li>
    ));
    return (
      <ul className="categories">
        {categoryElements}
      </ul>
    );
  }
}
CategoryList.propTypes = {
  category: PropTypes.objectOf(PropTypes.object),
  deleteCategory: PropTypes.func,
  deleteTodos: PropTypes.func,
  editCategory: PropTypes.func,
  plusCategory: PropTypes.func,
  match: PropTypes.objectOf(PropTypes.object),
  changeCurrentCategory: PropTypes.func,
};

CategoryList.defaultProps = {
  category: {},
  deleteCategory: () => undefined,
  deleteTodos: () => undefined,
  editCategory: () => undefined,
  plusCategory: () => undefined,
  match: {},
  changeCurrentCategory: () => undefined,
};


const mapStateToProps = store => ({ category: store.category });


export const mapDispatchToProps = dispatch => ({
  deleteCategory: item => dispatch(deleteCategoryItem(item)),
  editCategory: item => dispatch(editCategoryItem(item)),
  plusCategory: item => dispatch(plusCategoryItem(item)),
  changeCurrentCategory: item => dispatch(changeCurrentCategoryItem(item)),
  deleteTodos: item => dispatch(deleteTodoItems(item)),
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps,
)(CategoryList));
