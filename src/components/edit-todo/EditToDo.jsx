import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { Link } from 'react-router-dom';
import { editTodoItem } from '../todo/todoActions';

class EditToDo extends React.Component {
  handleActionClickEdit = () => {
    const { todo: updateToDo, editToDo } = this.props;
    updateToDo.title = this.inputTitle.value;
    updateToDo.description = this.inputDescription.value;
    editToDo(updateToDo);
  };

  render() {
    const { todo } = this.props;
    return (
      <form className="" action="#">
        <div className="form-group">
          <label>Title</label>
          <input className="form-control" id="title" type="text" required="required" value={todo.title} ref={(input) => { this.inputTitle = input; }} onChange={this.handleActionClickEdit} />
        </div>
        <div className="form-group">
          <label>Description</label>
          <textarea className="form-control" id="description" type="text" required="required" value={todo.description} ref={(input) => { this.inputDescription = input; }} onChange={this.handleActionClickEdit} />
        </div>
        <div className="form-group text-center">
          <Link to={`/categories/${todo.idCategory}`} className="link">
            <button type="button" className="btn btn-success btn-update" title="Click me to clear the input field">
              Update
            </button>
          </Link>
        </div>
      </form>
    );
  }
}
EditToDo.propTypes = {
  todo: PropTypes.objectOf(PropTypes.object),
  editToDo: PropTypes.func,
};

EditToDo.defaultProps = {
  todo: {},
  editToDo: () => undefined,
};

const mapStateToProps = (state, ownProps) => ({
  todo: state.todo.items.find(item => item.id === +ownProps.match.params.id),
});

const mapDispatchToProps = dispatch => ({
  editToDo: item => dispatch(editTodoItem(item)),
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps,
)(EditToDo));
