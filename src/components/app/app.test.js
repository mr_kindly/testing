/* eslint-disable react/jsx-filename-extension */
/* eslint-disable no-undef */
import * as React from 'react';
import Adapter from 'enzyme-adapter-react-16';
import { shallowToJson } from 'enzyme-to-json';
import { shallow, configure } from 'enzyme';
import App from './App';

configure({ adapter: new Adapter() });

describe('App', () => {
  it('renders without crashing', () => {
    const output = shallow(<App />);
    expect(shallowToJson(output)).toMatchSnapshot();
  });
});
