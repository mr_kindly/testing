/* eslint-disable react/jsx-filename-extension */
import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter } from 'react-router';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faEdit, faTrashAlt, faPlus, faAngleDown,
} from '@fortawesome/free-solid-svg-icons';
import Header from '../header/Header';
import ProgressLine from '../progress-line/ProgressLine';
import AddItem from '../add-item/AddItem';
import { addCategoryItem } from '../category/categoryActions';
import { addTodoItem } from '../todo/todoActions';
import Content from '../content/Content';
import './App.css';

library.add(faEdit, faTrashAlt, faPlus, faAngleDown);

class App extends PureComponent {
  render() {
    const {
      categories, addCategory, addTodo, history,
    } = this.props;
    return (
      <div className="app container">
        <header className="app-header">
          <Header />
        </header>
        <ProgressLine />
        <div className="row">
          <div className="col-xs-6 text-left">
            <AddItem action={{ title: 'Enter category title', action: 'Add', id: 'add-category' }} onButtonClick={addCategory} />
          </div>
          <div className="col-xs-6 text-right">
            <AddItem action={{ title: 'Enter todo title', action: 'Add', idCategory: history, id: 'add-todo' }} onButtonClick={addTodo} />
          </div>
        </div>
        <Content categories={categories.items} />
      </div>
    );
  }
}
App.propTypes = {
  categories: PropTypes.objectOf(PropTypes.object),
  addCategory: PropTypes.func,
  addTodo: PropTypes.func,
  history: PropTypes.arrayOf(PropTypes.array),
};

App.defaultProps = {
  categories: {},
  addCategory: () => undefined,
  addTodo: () => undefined,
  history: [],
};

const mapStateToProps = store => ({
  categories: store.category,
  todos: store.todo,
});

export const mapDispatchToProps = dispatch => ({
  addCategory: item => dispatch(addCategoryItem(item)),
  addTodo: item => dispatch(addTodoItem(item)),
});

export default withRouter(connect(
  mapStateToProps,
  mapDispatchToProps,
)(App));
