/* eslint-disable no-param-reassign */
import * as actionTypes from './actiontypes';

const initialState = {
  showDone: false,
  error: false,
  currentCategory: 0,
  items: [
    {
      id: 1,
      title: 'Search',
      idParent: 0,
      children: false,
      isOpen: true,
      isDone: false,
    },
    {
      id: 2,
      title: 'Update Todo',
      idParent: 0,
      children: false,
      isOpen: true,
      isDone: false,
    },
  ],
};

export default function categoryReducer(state = initialState, action) {
  switch (action.type) {
    case actionTypes.ADD_CATEGORY_ITEM:
    {
      const newState = {
        ...state,
        items: [
          {
            id: action.payload.id,
            title: action.payload.title,
            idParent: 0,
            isOpen: true,
            isDone: false,
          },
          ...state.items],
      };
      return newState;
    }
    case actionTypes.EDIT_CATEGORY_ITEM:
    {
      const updatedItems = state.items.map((item) => {
        if (item.id === action.payload.id) {
          item.title = action.payload.title;
        }
        return item;
      });
      return {
        ...state,
        items: [...updatedItems],
      };
    }
    case actionTypes.REMOVE_CATEGORY_ITEM:
    {
      const filterArray = state.items.filter(category => category.id !== action.payload);
      return {
        ...state,
        items: filterArray,
      };
    }
    case actionTypes.PLUS_CATEGORY_ITEM:
    {
      const updateChildProperty = state.items.map((item) => {
        if (item.id === action.payload.idParent) {
          item.children = true;
        }
        return item;
      });
      const newState = {
        ...state,
        items: [
          {
            id: action.payload.id,
            title: 'default',
            idParent: action.payload.idParent,
            isOpen: true,
            isDone: false,
          },
          ...updateChildProperty],
      };
      return newState;
    }
    case actionTypes.TOGGLE_SHOW_DONE_ITEM:
    {
      return {
        ...state,
        showDone: !action.payload,
      };
    }
    case actionTypes.DONE_CATEGORY_ITEM:
    {
      const updatedItems = state.items.map((item) => {
        if (item.id === action.payload.id) {
          item.isDone = action.payload.isDone;
        }
        return item;
      });
      return {
        ...state,
        items: [...updatedItems],
      };
    }
    case actionTypes.CURRENT_CATEGORY_ITEM:
    {
      return {
        ...state,
        currentCategory: action.payload,
      };
    }
    default:
      return state;
  }
}
