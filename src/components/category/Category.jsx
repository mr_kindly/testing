import React from 'react';
import PropTypes from 'prop-types';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import './category.scss';

class Category extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      visiableEdit: false,
    };
  }

  handleActionDeleteClick = () => {
    const { category, categoryList, onButtonClick } = this.props;
    let idCategory = category.id;
    const set = [];
    set.push(idCategory);
    while (set.length) {
      // eslint-disable-next-line no-loop-func
      set.push(...categoryList.items.filter(item => item.idParent === idCategory));
      onButtonClick.deleteCategory(idCategory);
      onButtonClick.deleteTodos(idCategory);
      idCategory = set.shift();
    }
  };

  handleActionEditClick = () => {
    this.setState({ visiableEdit: true });
  };

  handleActionEditChange = () => {
    const { category, onButtonClick } = this.props;
    category.title = this.input.value;
    onButtonClick.editCategory(category);
  };

  handleActionSaveEdit = () => {
    this.setState({ visiableEdit: false });
  };

  handleActionPlusClick = () => {
    const { category, categoryList, onButtonClick } = this.props;
    let max = 0;
    categoryList.items.forEach((element) => {
      if (element.id > max) {
        max = element.id;
      }
    });
    onButtonClick.plusCategory({ id: max + 1, idParent: category.id });
  };

  handleActionAngleClick = () => {
    const { category } = this.props;
    category.isOpen = !category.isOpen;
  };

  getChildren = () => {
    const { categoryList, category, onButtonClick } = this.props;
    const {
      deleteCategory, deleteTodos, editCategory, plusCategory,
    } = onButtonClick;
    let childrenArray = categoryList ? [...categoryList.items] : 0;
    childrenArray = childrenArray ? childrenArray
      .filter(item => item.idParent === category.id && category.isOpen === true)
      .map(item => (
        <li key={item.id}>
          <Link to={`/categories/${item.id}`} className="link">
            <Category
              category={item}
              onButtonClick={{
                deleteCategory, deleteTodos, editCategory, plusCategory,
              }}
              categoryList={categoryList}
            />
          </Link>
        </li>
      )) : '';
    return childrenArray;
  };

  render() {
    const { category } = this.props;
    const { visiableEdit } = this.state;
    const angleExist = category.children ? (
      <button
        className="btn btn-xs btn-link"
        onClick={this.handleActionAngleClick}
        type="button"
        tabIndex="0"
        onKeyDown={this.handleActionAngleClick}
      >
        <i className="glyphicon glyphicon-chevron-down" />
      </button>
    ) : '';
    return (
      <div>
        <div className="category">
          <div>
            {angleExist}
            <p id={`category-${category.id}`} className={visiableEdit ? 'none' : 'title-category'}>
              {category.title}
            </p>
            <input
              type="text"
              id={`editInput-${category.id}`}
              className={visiableEdit ? 'form-control edit-input' : 'none'}
              required="required"
              value={category.title}
              ref={(input) => { this.input = input; }}
              onChange={this.handleActionEditChange}
              onBlur={this.handleActionSaveEdit}
            />

          </div>
          <div>
            <button
              className="btn btn-default btn-xs btn-edit"
              onClick={this.handleActionEditClick}
              type="button"
              tabIndex="0"
              onKeyDown={this.handleActionEditClick}
            >
              <i className="glyphicon glyphicon-edit" />
            </button>
            <button
              className="btn btn-default btn-xs"
              onClick={this.handleActionDeleteClick}
              type="button"
              tabIndex="0"
              onKeyDown={this.handleActionDeleteClick}
            >
              <i className="glyphicon glyphicon-trash" />
            </button>
            <button
              type="button"
              className="btn btn-default btn-xs"
              onClick={this.handleActionPlusClick}
              tabIndex="0"
              onKeyDown={this.handleActionPlusClick}
            >
              <i className="glyphicon glyphicon-plus" />
            </button>
          </div>
        </div>
        <ul className="children">
          {this.getChildren()}
        </ul>
      </div>
    );
  }
}

Category.propTypes = {
  category: PropTypes.objectOf(PropTypes.object),
  categoryList: PropTypes.arrayOf(PropTypes.array),
  onButtonClick: PropTypes.objectOf(PropTypes.object),
};

Category.defaultProps = {
  category: {},
  categoryList: [],
  onButtonClick: {},
};

const mapStateToProps = store => ({
  categoryList: store.category,
});

export default connect(
  mapStateToProps,
)(Category);
