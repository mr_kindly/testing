/* eslint-disable no-undef */
import * as actions from './categoryActions';
import * as types from './actiontypes';

describe('actions', () => {
  it('should create an action to add a todo', () => {
    const title = 'new Category';
    const expectedAction = {
      type: types.ADD_CATEGORY_ITEM,
      payload: title,
    };
    expect(actions.addCategoryItem(title)).toEqual(expectedAction);
  });
});
describe('actions', () => {
  it('should create an action to edit a category', () => {
    const category = { id: 1, title: 'edit Category' };
    const expectedAction = {
      type: types.EDIT_CATEGORY_ITEM,
      payload: category,
    };
    expect(actions.editCategoryItem(category)).toEqual(expectedAction);
  });
});
