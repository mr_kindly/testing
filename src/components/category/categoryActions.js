import * as actionTypes from './actiontypes';

const addCategoryItem = category => ({
  type: actionTypes.ADD_CATEGORY_ITEM,
  payload: category,
});
const editCategoryItem = category => ({
  type: actionTypes.EDIT_CATEGORY_ITEM,
  payload: category,
});
const plusCategoryItem = category => ({
  type: actionTypes.PLUS_CATEGORY_ITEM,
  payload: category,
});
const deleteCategoryItem = id => ({
  type: actionTypes.REMOVE_CATEGORY_ITEM,
  payload: id,
});
const toggleShowDoneItem = flag => ({
  type: actionTypes.TOGGLE_SHOW_DONE_ITEM,
  payload: flag,
});
const changeDoneCategoryItem = value => ({
  type: actionTypes.DONE_CATEGORY_ITEM,
  payload: value,
});
const changeCurrentCategoryItem = id => ({
  type: actionTypes.CURRENT_CATEGORY_ITEM,
  payload: id,
});

export {
  addCategoryItem, editCategoryItem, plusCategoryItem, deleteCategoryItem, toggleShowDoneItem,
  changeDoneCategoryItem, changeCurrentCategoryItem,
};
