/* eslint-disable no-undef */
import reducer from './categoryReducer';

describe('categories reducer', () => {
  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(
      {
        showDone: false,
        error: false,
        currentCategory: 0,
        items: [
          {
            id: 1,
            title: 'Search',
            idParent: 0,
            children: false,
            isOpen: true,
            isDone: false,
          },
          {
            id: 2,
            title: 'Update Todo',
            idParent: 0,
            children: false,
            isOpen: true,
            isDone: false,
          },
        ],
      },
    );
  });
});
