import React from 'react';
import './emptyPage.scss';

const EmptyPage = () => (
  <h2>Select a category</h2>
);
export default EmptyPage;
