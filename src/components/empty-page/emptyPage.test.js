/* eslint-disable react/jsx-filename-extension */
/* eslint-disable no-undef */
import React from 'react';
import renderer from 'react-test-renderer';
import EmptyPage from './EmptyPage';

describe('Empty Page', () => {
  it('render Empty Page', () => {
    const tree = renderer
      .create(<EmptyPage />)
      .toJSON();
    expect(tree).toMatchSnapshot();
  });
});
