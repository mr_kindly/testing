import { combineReducers } from 'redux';
import categoryReducer from './components/category/categoryReducer';
import todoReducer from './components/todo/todoReducer';

const rootReducer = combineReducers({
  category: categoryReducer,
  todo: todoReducer,
});

export default rootReducer;
