/* eslint-disable no-undef */
/* eslint-disable no-underscore-dangle */
import { createStore, applyMiddleware, compose } from 'redux';
import rootReducer from './indexReducer';

let composeEnhancers = compose;
if (process.env.NODE_ENV !== 'production') {
  if (typeof window === 'object' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) {
    composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__;
  }
}
const store = createStore(rootReducer,
  composeEnhancers(
    applyMiddleware(
    ),
  ));
export default store;
