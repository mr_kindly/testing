/// <reference types="Cypress" />

context('Search', () => {
  beforeEach(() => {
    cy.visit('http://localhost:3000');
  });

  it('typing and checking value in search input', () => {
    const searchStr = 'Update title';

    cy.get('.search-control')
      .type(searchStr)
      .should('have.value', searchStr);
  });

  it('apply search and checking url', () => {
    const searchStr = 'Apply search';

    cy.get('.search-control')
      .type(searchStr)
      .should('have.value', searchStr)
      .get('.search-form')
      .submit();

    cy.location().should((location) => {
      expect(location.pathname).to.contain('/search/');
      expect(location.pathname).to.eq(`/search/${encodeURI(searchStr)}`);
    });
  });

  it('apply search and checking search results', () => {
    const searchStr = 'Apply search';

    cy.get('.search-control')
      .type(searchStr)
      .should('have.value', searchStr)
      .get('.search-form')
      .submit();

    cy.get('.todo-wrapper')
      .find('.todo')
      .should('contain', searchStr);
  });

  it('apply search with wrong query and checking search results(results should be "not found"', () => {
    const searchStr = 'Wrong search query';

    cy.get('.search-control')
      .type(searchStr)
      .should('have.value', searchStr)
      .get('.search-form')
      .submit();

    cy.get('.todo-wrapper')
      .should('contain', 'Search result is empty');
  });
});
