/// <reference types="Cypress" />

context('Add category', () => {
  beforeEach(() => {
    cy.visit('http://localhost:3000');
  });

  it('typing and adding new category to list of categories', () => {
    const newCategoryTitle = 'Add new category';
    cy.get('#add-category')
      .find('.form-control')
      .type(newCategoryTitle)
      .should('have.value', newCategoryTitle);

    cy.get('#add-category')
      .find('.btn')
      .click();

    cy.get('.categories')
      .find('.category')
      .contains(newCategoryTitle)
      .should('have.length.of.at.most', 1);
  });

});
