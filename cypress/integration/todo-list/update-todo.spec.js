/// <reference types="Cypress" />

context('Update todo', () => {
  beforeEach(() => {
    cy.visit('http://localhost:3000');
  });

  it('change title and description', () => {
    const newTitle = 'Update title';
    const newDescription = 'Update description';

    cy.get('#category-1')
      .click()
      .get('.todo-wrapper')
      .find('.todo')
      .first()
      .find('.todo-edit')
      .click()
      .get('#title')
      .clear()
      .type(newTitle)
      .get('#description')
      .clear()
      .type(newDescription)
      .get('.btn-update')
      .click();

    cy.get('.todo-wrapper')
      .find('.todo')
      .first()
      .should('contain', newTitle);
  });

  it('checkbox of todo should be checked after clicking on checkbox "is done"', () => {
    cy.get('#category-1')
      .click()
      .get('.todo-wrapper')
      .find('.todo')
      .first()
      .find('.todo-is-done')
      .click();

    cy.get('.todo-wrapper')
      .find('.todo')
      .first()
      .find('.todo-is-done')
      .should('be.checked');
  });
});
