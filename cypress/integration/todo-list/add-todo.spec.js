/// <reference types="Cypress" />

context('Add todo', () => {
  beforeEach(() => {
    cy.visit('http://localhost:3000');
  });

  it('typing and adding new todo to list of todos', () => {
    const newTitle = 'New todo';

    cy.get('#category-1')
      .click();

    cy.get('#add-todo')
      .find('.form-control')
      .type(newTitle)
      .should('have.value', newTitle);

    cy.get('#add-todo')
      .find('.btn')
      .click();

    cy.get('.todo-wrapper')
      .find('.todo')
      .should('contain', newTitle);
  });
});
