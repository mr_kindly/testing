/// <reference types="Cypress" />

context('Update category', () => {
  beforeEach(() => {
    cy.visit('http://localhost:3000');
  });

  it('should change title of category to input', () => {
    const newTitle = 'Update category';

    cy.get('.category')
      .first()
      .find('.btn-edit')
      .click();

    cy.get('.edit-input')
      .should('have.length', 1)
      .clear()
      .should('have.value', '')
      .type(newTitle)
      .should('have.value', newTitle)
      .blur()
      .get('.edit-input')
      .should('have.length', 0)
      .get('.category')
      .first()
      .should('contain', newTitle);
  });

});
